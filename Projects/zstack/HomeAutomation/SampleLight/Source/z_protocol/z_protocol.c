#include "z_protocol.h"
#include "hal_types.h"
#include "string.h"
#include "OSAL_Memory.h"
#include "OSAL.h"
#include "AF.h"
#include "ioCC2530.h"
#include "hal_uart.h"
#include "z_pro_app.h"
#include "my_socket.h"


#define ZSoftware_Protocol 0x03
#define ZHeartTicket_Protocol 0x03
#define ZProtocol_Version  0x01

z_protocol_info_t z_protocol_info;        
int set_timer_flag = 0;
uint8 target_timer[2]={0};
uint8_t Z_Verify_Data(uint8_t *pbuf, int len);

uint8_t Z_Verify_Data(uint8_t *pbuf, int len) //校验函数
{

    uint8_t cheaksum = 0;
    int i;
    ZDEV_PACKAGE* up_pack = (ZDEV_PACKAGE*)pbuf;

    uint16_t length = up_pack->Len_h*256 + up_pack->Len_l;

    for(i = 0;i< length-1;i++)
    {
        cheaksum ^= pbuf[i+2];
    }
    return cheaksum;
}

void end_Attributes_to_cood_Func(z_protocol_info_t *protocol_info , uint8 cmd)
{
    uint8_t product_id_len = strlen(protocol_info->product_id);
    uint8_t mac_id_len = strlen(z_protocol_info.IEEE_id);
    uint16_t length = 2 + 2 + 1 + product_id_len + 1 + Z_MAC_ADDR_LEN + 1 + 2 + 1 + 1; //length + 03+01+ p_id_len + p_id +MAC_len+ MAC +  shortadd——len+shortadd+cmd  + xor
    uint16_t packet_len = length + 4;
    
    
    uint8_t packet[70]={0};
    uint16_t offset = 0;

    ZDEV_PACKAGE *pack = (ZDEV_PACKAGE *)packet;
   
    packet[0]  =  UPKG_H80;
    pack->Head[1]  = UPKG_H7F;
    pack->Len_h = (uint8_t)(length>>8);
    pack->Len_l = (uint8_t)(length&0x00ff);
    if(cmd == end_hearstick_cmd)
    {
        pack->Protocol_Type  = ZHeartTicket_Protocol;
        pack->Protocol_Version   = ZProtocol_Version;
        offset += 6;
        packet[offset++] = product_id_len;
        memcpy(packet+offset,z_protocol_info.product_id,product_id_len);
        offset += product_id_len;

        packet[offset++] = mac_id_len;
        memcpy(packet+offset,z_protocol_info.IEEE_id,mac_id_len);
        offset += mac_id_len;
        
        uint16 my_shortaddr = NLME_GetShortAddr();
        packet[offset++] = 0x02;//短地址长度
        packet[offset++] = (uint8_t)(my_shortaddr>>8);
        packet[offset++] = (uint8_t)(my_shortaddr&0x00ff);
        
        packet[offset++] = cmd;
        
    }
    else if(cmd == connect_success_up_Attributes_Cmd)
    {
        pack->Protocol_Type  = ZSoftware_Protocol;
        pack->Protocol_Version   = ZProtocol_Version;
        offset += 6;
        packet[offset++] = 0;
        packet[offset++] = 0;
        packet[offset++] = 0;
        packet[offset++] = cmd;
        packet[offset++] = 1;
        packet[offset++] = 1;
        packet[offset++] = product_id_len;
        memcpy(packet+offset,z_protocol_info.product_id,product_id_len);
        offset += product_id_len;
        packet[offset++] = mac_id_len;
        memcpy(packet+offset,z_protocol_info.IEEE_id,mac_id_len);
        offset += mac_id_len;   
        packet[offset++] = 0x02;//shortaddr
        uint16 my_shortaddr = NLME_GetShortAddr();
        packet[offset++] = (uint8_t)(my_shortaddr>>8);
        packet[offset++] = (uint8_t)(my_shortaddr&0x00ff);
        packet_len += 5;
        length += 5;
        pack->Len_h = (uint8_t)(length>>8);
        pack->Len_l = (uint8_t)(length&0x00ff);
    }  
    
    packet[offset++] = Z_Verify_Data(packet,packet_len);//校验
    packet[offset++] = UPKG_T23 ;
    packet[offset] = UPKG_TDC;
    
     send_data_to_coo(packet_len,packet);
}

void end_status_to_coor(uint8 WhataData , uint8* data , uint8 data_len , uint8 cmd)
{
    uint8_t product_id_len = strlen(z_protocol_info.product_id);
    uint8_t mac_id_len = strlen(z_protocol_info.IEEE_id);
    uint16_t length = 2 + 1 + 1 + product_id_len +  mac_id_len + 2 + 1 + 2 + 1 + 1 + data_len + 1; 
    uint16_t packet_len = length + 4;
    
    uint8_t packet[80]={0};
    uint16_t offset = 0;
    
    ZDEV_PACKAGE *pack = (ZDEV_PACKAGE *)packet;
    
    packet[0]  =  UPKG_H80;
    pack->Head[1]  = UPKG_H7F;
    pack->Len_h = (uint8_t)(length>>8);
    pack->Len_l = (uint8_t)(length&0x00ff);
    pack->Protocol_Type  = ZSoftware_Protocol;
    pack->Protocol_Version   = ZProtocol_Version;
    offset += 6;
    
    packet[offset++] = product_id_len;
    memcpy(packet+offset,z_protocol_info.product_id,product_id_len);
    offset += product_id_len;

    packet[offset++] = mac_id_len;
    memcpy(packet+offset,z_protocol_info.IEEE_id,mac_id_len);
    offset += mac_id_len;
    
    uint16 my_shortaddr = NLME_GetShortAddr();
    packet[offset++] = 0x02;
    packet[offset++] = (uint8_t)(my_shortaddr>>8);
    packet[offset++] = (uint8_t)(my_shortaddr&0x00ff);
    
    
    packet[offset++] = cmd;
    
    packet[offset++] = WhataData;
    
    memcpy(packet+offset,data,data_len);
    offset += data_len;
    
    packet[offset++] = Z_Verify_Data(packet,packet_len);//校验
    packet[offset++] = UPKG_T23;
    packet[offset] = UPKG_TDC;
    
    send_data_to_coo(packet_len,packet);
    //osal_mem_free(packet);
  
}

void end_analysis_coor_Func(uint8 *buffer ,uint8 linkqi)
{
    uint16_t product_id_len = buffer[6];
    uint16_t device_id_len = buffer[6+product_id_len+1];
    uint8 packet_cmd = buffer[6+product_id_len+device_id_len+2+3];
    uint8* data = buffer+(6+product_id_len+device_id_len+2+3+1);
    
    if(packet_cmd == 0xD1)//判断是否app控制子设备
    { 
        if(data[0] == 0x01)
        {      
            control_socket(data[1],linkqi); 
        }
        else if(data[0] == 0x14)
        {
            set_timer_flag = 1;
            uint32 set_timer_num = (data[1]*256 + data[2])*60000;
            if(set_timer_num != 0x00)
            {
                osal_start_timerEx(SmartHome_TaskID,socket_set_timer,set_timer_num);
                target_timer[0] = data[3];
                target_timer[1] = data[4];
            }
            else
            {
                osal_stop_timerEx(SmartHome_TaskID,socket_set_timer);
                set_timer_flag = 0;
                memset(target_timer,0,2);
            }    
            uint8 socket_status1 = P1_7;
            uint8 status_lqi[5] ={socket_status1,linkqi,set_timer_flag,target_timer[0],target_timer[1]};
            end_status_to_coor(0x1B,status_lqi,5,Dev_status_Cmd);
        }
    }
    else if(packet_cmd == 0xB1)
    {
          uint8 socket_status1 = P1_7;
          uint8 status_lqi[5] ={socket_status1,linkqi,set_timer_flag,target_timer[0],target_timer[1]};
          end_status_to_coor(0x1B,status_lqi,5,Dev_status_Cmd);        
    }
    else if(packet_cmd == 0x3A)
    {
        end_Attributes_to_cood_Func(&z_protocol_info , end_hearstick_cmd);
    }
    else if(packet_cmd == 0x3B)
    {
        int temp_time = (osal_rand()*100)%8000;
        osal_start_timerEx(SmartHome_TaskID,SmartHome_shortaddr_SEND,temp_time);
    }
    else if(packet_cmd == 0xB2)
    {
        end_status_to_coor(0x01,&linkqi,1,0xB3);
    }
}
