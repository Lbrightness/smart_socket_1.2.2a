
#ifndef __Z_PROTOCOL__
#define __Z_PROTOCOL__

#include "hal_types.h"
#include "AF.h"
typedef uint8 uint8_t;
typedef uint16 uint16_t;
typedef uint32 uint32_t;

#define UPKG_H7F         0x7F
#define UPKG_H80         0x80
#define UPKG_TDC         0xDC
#define UPKG_T23         0x23

#define Z_MAC_ADDR_LEN    16
#define SHORTADDR_LEN   2

#define z_contrl         0x01
#define z_statu          0x02
#define z_contrl_num     0x04

#ifndef DEV_PRODUCT_LENGTH
	#define DEV_PRODUCT_LENGTH 64
#endif

#ifndef DEV_IEEE_LENGTH
	#define DEV_IEEE_LENGTH 17
#endif


typedef enum
{
    connect_success_up_Attributes_Cmd 	      = 0x34,     //连接成功，上报属性
    Dev_status_Cmd	                          = 0xC1,
    end_hearstick_cmd                         = 0xA0,
    end_bind_Cmd                              = 0xE1,
    end_bind_ack_Cmd                          = 0xE2,
}z_CmdTypeDef;




typedef struct 
{
    char               product_id[DEV_PRODUCT_LENGTH];         //产品id
    char 	       IEEE_id[DEV_IEEE_LENGTH];               //IEEE
    uint16_t           shortaddr;

}z_protocol_info_t;

typedef struct
{
    uint8_t Head[2];
    uint8_t Len_h;
    uint8_t Len_l;
    uint8_t         	Protocol_Type;
    uint8_t		Protocol_Version;
    uint8_t             para[256];//len + dev_product_id + len + dev_mac + parameter + XOR + ENDING(23,DC)
}ZDEV_PACKAGE;

extern z_protocol_info_t z_protocol_info;
extern uint8 temp_password[11];
extern int set_timer_flag ;
extern uint8 target_timer[2];
extern void end_Attributes_to_cood_Func(z_protocol_info_t *protocol_info, uint8 cmd);
extern uint8_t Z_GetPacketCmd_Func(uint8 *buff);
extern uint8_t* cood_analysis_end_Attributes_Func(uint8 *buff);
extern void end_analysis_coor_Func(uint8 *buffer ,uint8 linkqi );
extern void end_status_to_coor(uint8 WhataData ,  uint8* data , uint8 data_len , uint8 cmd);


#endif