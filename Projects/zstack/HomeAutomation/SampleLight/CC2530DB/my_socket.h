#ifndef __MY_SOCKET_H__
#define __MY_SOCKET_H__

#define MY_SOCKET_ENDPOINT           20//1~240
#define MY_SOCKET_PROFID             0x0F08
#define MY_SOCKET_DEVICEID           0x0001
#define MY_SOCKET_DEVICE_VERSION     0
#define MY_SOCKET_FLAGS              0
#define MY_SOCKET_MAX_INCLUSTERS     1
#define SmartHome_CLUSTERID          1

#define SEND_STATUS                   0x0002
#define SmartHome_HeartTicket_SEND    0x0004
#define STOP_JOIN_NWK                 0x0008
#define LED_Blinking_3s               0x0010
#define relay_delay                   0x0020
#define SmartHome_shortaddr_SEND      0x0040
#define socket_set_timer              0x0080

#define GATEWAY_GROUP         0x0253 //���

#include "ZDProfile.h"
void My_Light_Init( byte task_id );
void rxCB(uint8 port,uint8 event);
UINT16 My_Light_event_loop( byte task_id, UINT16 events );
void MY_SOCKET_deal_CoorData(afIncomingMSGPacket_t *pkt);
void socket_stuts_send(uint8 link_quality);

extern byte SmartHome_TaskID;
extern byte SmartHome_TransID;
extern endPointDesc_t end_epDesc;
extern void control_socket(bool ret,uint8 link_quality);
extern void send_data_to_coo( uint16 len, uint8 *buf);

#endif