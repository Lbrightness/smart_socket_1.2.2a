#include "ioCC2530.h"
#include "OSAL.h"
#include "ZDApp.h"
#include "ZDObject.h"
#include "MT_SYS.h"
#include "MT_UART.h"
#include "MT.h"
#include "nwk_util.h"
#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_diagnostic.h"
#include "z_pro_app.h"
#include "z_protocol.h"

#include "hal_lcd.h"
#include "hal_led.h"
#include "hal_key.h"
#include "hal_uart.h"
#include "hal_drivers.h"
#include "NLMEDE.h"
#include "string.h"
#include "OnBoard.h"
#include "onboard.h"
#include "my_socket.h"

byte SmartHome_TaskID;
byte SmartHome_TransID;
endPointDesc_t end_epDesc;
devStates_t SmartHome_NwkState;
unsigned char uartbuf[30];

void Deal_KeyChange_func(uint8 shift, uint8 keys);
void Delay(unsigned int n)  ;
void relay_led_init(void);

const cId_t MY_SOCKET_InClusterList[1]=
{
  SmartHome_CLUSTERID
};

const SimpleDescriptionFormat_t my_socket_SimpleDesc =
{
  MY_SOCKET_ENDPOINT,                  
  MY_SOCKET_PROFID,                    
  MY_SOCKET_DEVICEID,
  MY_SOCKET_DEVICE_VERSION,            
  MY_SOCKET_FLAGS,                    
  MY_SOCKET_MAX_INCLUSTERS,         
  (cId_t *)MY_SOCKET_InClusterList, 
  MY_SOCKET_MAX_INCLUSTERS,       
  (cId_t *)MY_SOCKET_InClusterList 
};

void My_Light_Init( byte task_id )
{
  SmartHome_TaskID = task_id;
  SmartHome_TransID = 0;
 
  halUARTCfg_t uartConfig; 
  uartConfig.configured  = TRUE;
  uartConfig.baudRate    = HAL_UART_BR_115200;
  uartConfig.flowControl = FALSE;
  uartConfig.callBackFunc= rxCB;
  HalUARTOpen(0,&uartConfig);
  
  end_epDesc.endPoint = MY_SOCKET_ENDPOINT;
  end_epDesc.task_id = &SmartHome_TaskID;
  end_epDesc.simpleDesc = (SimpleDescriptionFormat_t *)&my_socket_SimpleDesc;
  end_epDesc.latencyReq = noLatencyReqs;
  int result = afRegister(&end_epDesc);
  
  z_peotocol_init(&z_protocol_info,socket_product_id);                 //赋值mac，shortaddr，
  
 
  relay_led_init(); //初始化继电器
  osal_start_timerEx(SmartHome_TaskID,STOP_JOIN_NWK,60000);
  RegisterForKeys(SmartHome_TaskID);//注册按键
 //osal_start_timerEx(SmartHome_TaskID,relay_delay,5000); 
}
     
UINT16 My_Light_event_loop( byte task_id, UINT16 events )
{
  
  afIncomingMSGPacket_t  *MSGpkt;
  (void)task_id;
 
  if( events & SYS_EVENT_MSG )
  {
    while( (MSGpkt = (afIncomingMSGPacket_t *)osal_msg_receive(SmartHome_TaskID) ) )
    {
      switch( MSGpkt->hdr.event )
      {
        case AF_INCOMING_MSG_CMD:      
          MY_SOCKET_deal_CoorData(MSGpkt);
        break;
        
         case ZDO_STATE_CHANGE:
          SmartHome_NwkState = (devStates_t)(MSGpkt->hdr.status);
          if(SmartHome_NwkState == DEV_ROUTER ) //入网成功
          {
             //ZDApp_StopJoiningCycle();
            // HalUARTWrite(0,"enddevice into nwk\n",19);
            //NLME_PermitJoiningRequest(0); //禁止入网
            osal_stop_timerEx(SmartHome_TaskID,STOP_JOIN_NWK);
            end_Attributes_to_cood_Func(&z_protocol_info ,connect_success_up_Attributes_Cmd);
            osal_start_timerEx(SmartHome_TaskID,SmartHome_HeartTicket_SEND,3000);
                 uint8 led_times = 6;
                 while(led_times--)
                 {
                   Delay(3);
                   P2_2 = ~P2_2;
                   Delay(3);
                   P2_2 = ~P2_2;
                 }
          }
          else if(SmartHome_NwkState == DEV_NWK_ORPHAN )//没有协调器消息
          {
               // HalUARTWrite(0,"enddevice outto nwk\n",19);
          }
          
          case AF_DATA_CONFIRM_CMD:                                    //只要调用了了AF函数，就会触发这个事件
          break;
          
         case KEY_CHANGE:
           
           Deal_KeyChange_func(((keyChange_t *)MSGpkt)->state, ((keyChange_t *)MSGpkt)->keys);
           break;
          
          default:
            break;
      }
      osal_msg_deallocate((uint8*)MSGpkt);
    }
    return(events ^ SYS_EVENT_MSG);
  }
  
  if(events & SmartHome_HeartTicket_SEND)
  {
     end_Attributes_to_cood_Func(&z_protocol_info , end_hearstick_cmd);//发送属性给协调器
     osal_start_timerEx(SmartHome_TaskID,SmartHome_HeartTicket_SEND,20000);//15s发送一次心跳包
    return(events ^ SmartHome_HeartTicket_SEND);
  }

  if(events & STOP_JOIN_NWK)
  {
      ZDApp_StopJoiningCycle();
      return(events ^ STOP_JOIN_NWK);
  }
  
  if(events & SEND_STATUS)
  {
      socket_stuts_send(0);   
      return(events ^ SEND_STATUS);
  }
  if(events & relay_delay)
  {
       RegisterForKeys(SmartHome_TaskID);//注册按键
      return(events ^ relay_delay);
  }
  if(events & LED_Blinking_3s)
  {
      uint8 led_times1_3s = 3;
      while(led_times1_3s--)
      {
          Delay(5);
          P2_2 =  ~P2_2; 
          Delay(5);
          P2_2=  ~P2_2;
      }
      return(events ^ LED_Blinking_3s);
  }
  if(events & SmartHome_shortaddr_SEND)
  {  
        end_Attributes_to_cood_Func(&z_protocol_info ,connect_success_up_Attributes_Cmd);//发送入网
        return(events ^ SmartHome_shortaddr_SEND);
  }
  if(events & socket_set_timer)
  {  
        P1_7 = ~P1_7;
        P2_2 = ~P2_2;
        set_timer_flag = 0;
        memset(target_timer,0,2);
        uint8 socket_status1 = P1_7;
        uint8 status_lqi[4] ={socket_status1,set_timer_flag,target_timer[0],target_timer[1]};
        end_status_to_coor(0x19,status_lqi,4,Dev_status_Cmd);
        return(events ^ socket_set_timer);
  }
   return 0;
}

void MY_SOCKET_deal_CoorData(afIncomingMSGPacket_t *pkt)
{

  switch(pkt->clusterId)
  {
    case SmartHome_CLUSTERID:
      //HalUARTWrite(0,pkt->cmd.Data,pkt->cmd.DataLength);
      end_analysis_coor_Func(pkt->cmd.Data,pkt->LinkQuality);  
      
    break;  
  }
}
void Deal_KeyChange_func(uint8 shift, uint8 keys)
{
  
  if ( keys & HAL_KEY_SW_9 )
 {
      if(shift == 1)
      {
        P1_7 = ~P1_7;
        P2_2 = ~P2_2;
        
        uint8 socket_status;
        if( P1_7 == 1)
        {
          socket_status =1;
        }
        else if(P1_7 == 0)
        {
          socket_status =0;
        }
        end_status_to_coor(0x01,&socket_status,1,Dev_status_Cmd);
      }
      else if(shift == 2)
      {     
           zgWriteStartupOptions(ZG_STARTUP_SET, ZCD_STARTOPT_DEFAULT_NETWORK_STATE);
           SystemReset();
      }
  }   
}

void relay_led_init(void)
{
  /******LED*******/
  P2DIR |= 0x04;//输入（0）还是输出（1）
  P2_2 = 1;
  
  /******Relay*******/
  P1SEL &= ~0x80; //通用（0）还是外设（1）
  P1DIR |= 0x80;//输入（0）还是输出（1）
  P1_7 = 0;
  
}

void control_socket(bool ret ,uint8 link_quality)
{
    P1_7 = ret;
    P2_2 = !(ret);
    uint8 socket_status[2] ={ret,link_quality};
    end_status_to_coor(0x03,socket_status,2,Dev_status_Cmd);
}

void socket_stuts_send(uint8 link_quality)
{
    uint8 socket_status1 = P1_7;
    uint8 status_lqi[2] ={socket_status1,link_quality};
    end_status_to_coor(0x03,status_lqi, 2 ,Dev_status_Cmd);
}

static void rxCB(uint8 port,uint8 event)
{
    uint16 length;
    osal_memcpy(uartbuf,0,30);
    length = Hal_UART_RxBufLen(0);
    if(length)
    {
       HalUARTRead(0,uartbuf,length);
       HalUARTWrite(0,uartbuf,length);
       afAddrType_t DstAddr;
        DstAddr.addrMode = (afAddrMode_t)Addr16Bit;
        DstAddr.endPoint = 20;//endPoint
        DstAddr.addr.shortAddr = 0x0000;
       AF_DataRequest(&DstAddr,&end_epDesc,SmartHome_CLUSTERID,length,uartbuf,&SmartHome_TransID,AF_DISCV_ROUTE,AF_DEFAULT_RADIUS);
    }
    
}

void send_data_to_coo( uint16 len, uint8 *buf)
{
    afAddrType_t DstAddr;
    DstAddr.addrMode = (afAddrMode_t)AddrGroup;
    DstAddr.endPoint = MY_SOCKET_ENDPOINT;
    DstAddr.addr.shortAddr = GATEWAY_GROUP;
    
    AF_DataRequest(&DstAddr,&end_epDesc,SmartHome_CLUSTERID,len,buf,&SmartHome_TransID,AF_DISCV_ROUTE,AF_DEFAULT_RADIUS);
}

void Delay(unsigned int n)  
{
  unsigned char i;
  unsigned int j;
  for(i = 0; i < n; i++)
    for(j = 1; j; j++)
    ;
} 